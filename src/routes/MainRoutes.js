import * as React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import {AntDesign} from '@expo/vector-icons';
import Home from "../screens/Home";
import Search from "../screens/Search";
import Artist from "../screens/Artist";


const Tab = createBottomTabNavigator();
const HomeStack = createStackNavigator();

function HomeStackScreen() {
    return (
        <HomeStack.Navigator>
            <HomeStack.Screen name="Home" component={Home}/>
            <HomeStack.Screen name="Artist" component={Artist}/>
        </HomeStack.Navigator>
    );
}

const MainRoutes = () => {

    return (
        <Tab.Navigator
            screenOptions={({route}) => ({
                tabBarIcon: ({focused, color, size}) => {
                    let iconName;

                    if (route.name === 'Home') {
                        iconName = 'home';
                    } else if (route.name === 'Search') {
                        iconName = 'search1';
                    }
                    return <AntDesign name={iconName} size={size} color={color}/>;
                },
            })}
            tabBarOptions={{
                activeTintColor: '#581845',
                inactiveTintColor: 'gray',
            }}>
            <Tab.Screen name="Home" component={HomeStackScreen}/>
            <Tab.Screen name="Search" component={Search}/>
        </Tab.Navigator>
    );
};

export default MainRoutes