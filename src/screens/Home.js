import React, {useEffect, useState} from 'react';
import {StyleSheet, ActivityIndicator, FlatList} from "react-native";
import PopularSong from "../components/PopularSong";
import {apiKey, format} from "../../configApp";


const Home = ({navigation}) => {
    const [tracks, setTracks] = useState({})
    const [isLoading, setLoading] = useState(true);
    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch(`http://ws.audioscrobbler.com/2.0/?method=chart.gettoptracks&api_key=${apiKey}&format=${format}`);
            const data = await response.json();
            setTracks(data);
            setLoading(false)
        };
        fetchData();
    }, [])
    return (
        <>
            {isLoading ? <ActivityIndicator size='large' color='blue'/> :
                (<FlatList
                    keyExtractor={(item, index) => item.mbid + index}
                    data={tracks.tracks.track && tracks.tracks.track}
                    renderItem={({item}) => <PopularSong {...{item, navigation}} />}
                />)
            }
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    }
});


export default Home;