import React, {useState} from 'react';
import {StyleSheet, TextInput, View, ActivityIndicator, ScrollView} from "react-native";
import {apiKey, format} from "../../configApp";
import SearchedSong from "../components/SearchedSong";



const Search = () => {
    const [searchValue, setSearchValue] = useState('');
    const [searchData, setSearchData] = useState([]);
    const [isLoading, setLoading] = useState(false);

    const handleSearch = () => {
        setLoading(true)
        const fetchData = async () => {
            const response = await fetch(`http://ws.audioscrobbler.com/2.0/?method=track.search&track=${searchValue}&api_key=${apiKey}&format=${format}`);
            const data = await response.json();
            let tracksArr =  data.results.trackmatches.track
            setSearchData(tracksArr);
            setLoading(false)
        };
        fetchData();
    }
    return (
        <>
            <View style={styles.searchBlock}>
                <TextInput
                    style={styles.searchInput}
                    placeholder="Search song..."
                    onChangeText={text => setSearchValue(text)}
                    defaultValue={searchValue}
                    onBlur={handleSearch}
                />
            </View>
            {isLoading ? <ActivityIndicator size='large' color='blue'/> :
                (<View style={styles.container}>
                    <ScrollView contentContainerStyle={styles.wrapper}>
                        {searchData && searchData.map(e => <SearchedSong song={e}/>)}
                    </ScrollView>
                </View>)}

        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    searchBlock: {
        paddingTop: 60,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    searchInput: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 5,
        width: "80%",
    },
    wrapper: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 80
    }
});
export default Search;