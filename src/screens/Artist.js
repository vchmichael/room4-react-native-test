import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, ActivityIndicator, Image, ScrollView} from "react-native";
import {apiKey, format} from "../../configApp";
import Tags from "../components/Tags";


const Artist = ({route}) => {
    const { artistName } = route.params
    const [isLoading, setLoading] = useState(true);
    const [artist, setArtist] = useState({})
    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch(`http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=${artistName}&api_key=${apiKey}&format=${format}`);
            const data = await response.json();
            setArtist(data);
            setLoading(false)
        };
        fetchData();
    }, [])


    return (
        <>
            {isLoading ? <ActivityIndicator size='large' color='blue'/> :
                (<View style={styles.container}>
                    <ScrollView contentContainerStyle={styles.wrapper}>
                    <Image
                        style={styles.tinyLogo}
                        source={{
                            uri: `${artist.artist.image[2]['#text']}`,
                        }}/>
                    <Text style={styles.artistName} >{artistName}</Text>
                    <Text style={styles.artistBio} >{artist.artist.bio.summary}</Text>
                        <View style={styles.tagsBlock}>
                            {artist.artist.tags.tag.map(tag => <Tags tag={tag}/>)}
                        </View>
                    </ScrollView>
                </View>)}
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
    },
    tinyLogo: {
        width: 200,
        height: 200,
    },
    artistName: {
        fontSize: 20,
        fontWeight: 'bold',
        margin: 20
    },
    wrapper: {
        flex: 1,
        width: '80%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    artistBio: {
        textAlign: 'justify'
    },
    tagsBlock: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    }
});

export default Artist;