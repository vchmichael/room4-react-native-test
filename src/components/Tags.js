import React from 'react';
import * as WebBrowser from "expo-web-browser";
import {TouchableOpacity, StyleSheet, Text} from "react-native";

function Tags({tag}) {
    const handleOpenLink = () => {
        WebBrowser.openBrowserAsync(`${tag.url}`)
    }
    return (
        <TouchableOpacity onPress={handleOpenLink}>
            <Text style={styles.tagText}>#{tag.name}</Text>
        </TouchableOpacity>
    );
}
const styles = StyleSheet.create({
    tagText: {
        fontSize: 12,
        color: "#581845",
        fontWeight: "bold",
        alignSelf: "center",
        textTransform: "uppercase",
        padding: 10
    }
});

export default Tags;