import React from 'react';
import {View, StyleSheet, Image, Text, Button, TouchableOpacity} from "react-native";
import * as WebBrowser from 'expo-web-browser';
import MarqueeText from 'react-native-marquee';




const PopularSong = ({navigation, item}) => {
    const handleOpenLink = () => {
        WebBrowser.openBrowserAsync(`${item.artist.url}`)
    }
    return (
        <View style={styles.mainBlock}>
                <Image
                    style={styles.tinyLogo}
                    source={{
                        uri: `${item.image[0]['#text']}`,
                    }}/>
            <View style={styles.infoBlock}>
                <MarqueeText
                    style={styles.songName}
                    duration={3000}
                    marqueeOnStart
                    loop
                    marqueeDelay={1000}
                    marqueeResetDelay={1000}
                >
                    {item.name}
                </MarqueeText>
                <TouchableOpacity onPress={() =>
                    navigation.push('Artist', {
                        artistName: item.artist.name,
                    })
                }>
                    <Text style={styles.artistName}>{item.artist.name}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={handleOpenLink} >
                    <Text style={styles.lastFmText}>Last.fm</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    mainBlock: {
        flexDirection: 'row',
        backgroundColor: '#FFC300',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderRadius: 5,
        margin: 10,

    },
    infoBlock: {
        alignItems: 'flex-end',
        flex: 1
    },
    tinyLogo: {
        width: 80,
        height: '100%',
    },
    songName: {
        fontSize: 14,
        fontWeight: "bold",
        padding: 5,
        color: '#9C9C9C',
        textTransform: 'uppercase',
    },
    lastFmText: {
        color: '#FF5733',
        padding: 5
    },
    artistName: {
        color: '#C70039',
        padding: 5
    }
});

export default PopularSong;