import React from 'react';
import {StyleSheet, Text, View} from "react-native";


const SearchedSong = ({song}) => {
    return (
        <View style={styles.mainBlock}>
            <Text>
                {song.name} - {song.artist}
            </Text>
        </View>
    );
}
const styles = StyleSheet.create({
    mainBlock: {
        width: '80%',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        borderRadius: 5,
        margin: 20,
        borderBottomColor: '#FADA11',
        borderBottomWidth: 2,
    }
});
export default SearchedSong;