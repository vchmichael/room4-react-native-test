import {StatusBar} from 'expo-status-bar';
import React from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import MainRoutes from "./src/routes/MainRoutes";



export default function App() {
    return (
        <NavigationContainer>
            <MainRoutes/>
            <StatusBar style="auto"/>
        </NavigationContainer>

    );
}

